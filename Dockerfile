FROM openjdk
COPY src JavaDocker
EXPOSE 80
WORKDIR JavaDocker
RUN mkdir -p bin
RUN javac -d bin ./com/myApp/HelloWorld.java
WORKDIR bin
CMD ["java", "com.myApp.HelloWorld"]

FROM nginx
COPY ./index.html /usr/share/nginx/html

